/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.5.5-10.1.36-MariaDB : Database - rohinga
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rohinga` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rohinga`;

/*Table structure for table `biometric` */

DROP TABLE IF EXISTS `biometric`;

CREATE TABLE `biometric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` varchar(255) DEFAULT NULL,
  `r_id` varchar(255) DEFAULT NULL,
  `photo` mediumblob,
  `signature` mediumblob,
  `wsq_rt` mediumblob,
  `wsq_ri` mediumblob,
  `wsq_rm` mediumblob,
  `wsq_rr` mediumblob,
  `wsq_rl` mediumblob,
  `wsq_lt` mediumblob,
  `wsq_li` mediumblob,
  `wsq_lm` mediumblob,
  `wsq_lr` mediumblob,
  `wsq_ll` mediumblob,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `biometric` */

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fld1` varchar(25) DEFAULT NULL,
  `fld2` varbinary(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `test` */

insert  into `test`(`id`,`fld1`,`fld2`) values (1,'ssss','ffff'),(2,'wwww','eeee');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`) values (1,'idea','idea@idea.com',''),(3,'xxxx','zxzxzx@xx.com','xxxx');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
