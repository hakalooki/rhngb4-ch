<!DOCTYPE html>
<html lang="en">
<head>
	<link href = "<?php echo base_url();?>assets/css/bulma.min.css" rel = "stylesheet">
	<script src = "<?php echo base_url();?>assets/js/jquery.min.js"></script>
</head>
<body>
	<div class = "container">
		<h3 align = "center">Import Multiple Images</h3>
		 <br />

    <form id="import_form" method="post" enctype="multipart/form-data">
       
        <input type="file" name="image[]" id="image" multiple accept=".jpg, .png, .gif, .wsq"/>
        <input type="submit" name="import" id="import" value="Import"/>

    </form>

   </div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$('#import_form').on('submit', function(event){
			//event.preventdefault();
			var img_name = $('#image').val();
			//alert(img_name);
			//return false;
			if(img_name == '') {
				alert('Please Select Images');
				return false;
			}
			else {
				$.ajax({
					url: "<?php echo base_url();?>index.php/import/insert_images",
					method: "post",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData: false,
					success: function(data) {
						alert(data);
						$('#image').val('');

						//alert('data');
					}
				});
			}
		});

	});
</script>