<!DOCTYPE html>
<html lang="en">
<head>
	<link href = "<?php echo base_url();?>assets/css/bulma.min.css" rel = "stylesheet">
	<script src = "<?php echo base_url();?>assets/js/jquery.min.js"></script>
</head>
<body>
	<div class = "container">
		<h3 align = "center">Import Multiple Images</h3>
		 <br />

    <form action="<?php echo base_url();?>index.php/images/insert_images" method="post" id="import_form" enctype="multipart/form-data">
       
        <input type="file" name="image[]" id="image" multiple accept=".jpg, .png, .gif, .wsq"/>
        <input type="submit" name="import" id="import" value="Import"/>

    </form>


    <?php 

					if($status != '' )
					{
					?>
					<div class="page-title">
						<div class="title_left">
						 <?php 
						 if($status == "success")
						 {
						 ?>
							<div class = "successful_msg">
								 <p> Import Successful..!! </p>
							</div>
						 <?php 
						 }
						 else if($status == "error") 
						 {
						 ?>  
							<div class = "failed_msg">
								  <p>No File Selected</p>      
							</div>
						 <?php 
						 }
						 else
						 {
						 ?>  
							<div class = "validation_msg">   
								  <?php echo validation_errors();?>  
							</div>
					     <?php
						 }
						 ?>  
						</div>
						<div class="title_right"></div>
					</div>
					<?php 
					}
					?>



   </div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		

	});
</script>