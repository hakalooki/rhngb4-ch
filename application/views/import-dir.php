<!DOCTYPE html>
<html lang="en">

<?php
if (isset($this->session->userdata['logged_in'])) {
	$username = ($this->session->userdata['logged_in']['username']);
	$email = ($this->session->userdata['logged_in']['email']);
} else {
	header("location: http://localhost/rhng/index.php/auth/user_login");
}
?>

<head>
	<title>Import Form</title>
	<link href = "<?php echo base_url();?>assets/css/bulma.min.css" rel = "stylesheet">
	<script src = "<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>
<body>
	<section class="hero is-large">
	  <div class="hero-head">
	    <nav class="navbar">
	      <div class="container">
	        <div class="navbar-brand">

	          <a class="navbar-item">
	            <img class="image is-64x64" src="<?php echo base_url();?>assets/img/ec_logo.png" width="" height="">
	          </a>
	          
	        </div>




	        <div id="navbarMenuHeroB" class="navbar-menu">
	          <div class="navbar-end">
	            <div class="navbar-itemxx">
	             <?php
	             	echo '<span class="has-text-bold has-text-success is-size-5">'.$this->session->userdata['logged_in']['username'].'</span>';
	             	echo '<br />';
	             	echo '<p class="has-text-grey has-text-centered is-size-6">'.$this->session->userdata['logged_in']['email'].'</p>';

	              ?>
	            </div>

	            <div class="navbar-item"></div>
	            <div class="navbar-item"></div>
	            
	            <span class="navbar-item">
	              <a class="button is-danger is-inverted" href="<?php echo base_url();?>index.php/auth/logout">
	                <span class="icon">
	                  <i class="fab fa-github"></i>
	                </span>
	                <span>Logout</span>
	              </a>
	            </span>
	          </div>
        	</div>






	      </div>
	    </nav>
	  </div>

	  <div class="hero-body">

	    <div class="container has-text-centered">
	    	<form action="<?php echo base_url();?>index.php/images/insert_images_dir" method="post" id="import_form">
		      <div class="field has-addons has-addons-centered">
					  <div class="control">
					    <input class="input" type="text" name="folder" id="folder" placeholder="Enter a Directory Path">
					  </div>
					  <div class="control">
					  	<!--span>
								<a id="load" class="button is-success is-loading" hidden>Loading</a>
							</span-->
					    <button class="button is-success" type = "submit" name="import" id="import">
						    <span class="icon" id="load">
								  <i class="fas fa-pulse"></i>
								</span>
								<span>Import</span>
							</button>
					  </div>
					</div>

				</form>
				
				

				<?php 

				if (isset($this->session->userdata['import'])) {
					if($this->session->userdata['import']['cnt'] > 0)
						echo '<p class="has-text-success has-text-centered is-size-5">'.$this->session->userdata['import']['cnt']. ' data imported</p>';

					if ($this->session->userdata['import']['dp'] > 0)
						echo '<p class="has-text-danger has-text-centered is-size-5">'.$this->session->userdata['import']['dp']. ' data already exits</p>';
				}

				 //echo $this->benchmark->elapsed_time('', '', 2);

				?>

				{elapsed_time}

	    </div>
	  </div>

	  <div class="hero-foot">
	  	<p class="has-text-danger has-text-centered is-size-7">
				&copy; বাংলাদেশ নির্বাচন কমিশন
			</p>
	  </div>
	</section>

</body>
</html>









































<script type="text/javascript">
	$(document).ready(function(){
		$('#load').hide();

		$('#import_form').on('submit', function(event){
			$('#load').show();
		});
		

	});
</script>