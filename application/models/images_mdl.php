<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
	class Images_mdl extends CI_model{
			
			
		function __construct()
		{
			//
		}

		function redundancy_check($table, $field, $item)
		{
			$query = $this -> db -> select( $field )
								 -> from( $table )
								 -> get();
			 $temp_new = strtolower( preg_replace('/\s+/', '', $item));
			 foreach($query -> result() as $info):
				$temp_old = strtolower( preg_replace('/\s+/', '',$info -> $field));
				if($temp_old == $temp_new) return true;
			 endforeach;
			 
			 return false;
		}


		function get_images_by_id($id)
		{
			//$iid = 'xx';

			$query = $this->db->select('id, img')
							-> from('biometric')
							-> where('rid', $id)
							-> get();
			
			return $query->result();
			//return $query;
			//return $query->result_array();
		}


		function insert_images($data)
		{
			

			$this->db->set($data);
			$this->db->insert($this->db->dbprefix . 'biometric');
			//$this -> db -> insert('biometric');

			//$this -> db -> insert('biometric', $data);
		}


		function insert_images_dir()
		{

			$path = $this->input->post('folder');

			$d = $this->prepare_data($path);

			$this->db->insert_batch('biometric', $d['rs']);

			return $d['cnt'];
		}

		function insert_images_dir_single()
		{

			$fp = $this->input->post('folder');

			$this->load->helper('directory');

			//$f = 'C:\Users\EC\Desktop\10120170911203716';
			$cnt = 0;
			$dp = 0;
			$rs = array();
			$data = array();

			$map = directory_map($fp);

			foreach ($map as $sf => $files) {
				$sfn = strstr($sf, '\\', TRUE);

				$exists = $this->redundancy_check('biometric','r_id',$sfn);

				if(!$exists) {
					$d = $this->prepare_data($fp, $sf, $files);

					$this->db->set($d);
					$this->db->insert($this->db->dbprefix . 'biometric');

					$cnt++;
				} else {
					$dp++;
				}
			}

			$sess_data = array(
					'cnt' => $cnt,
					'dp' => $dp,
					'status' => 'success'
					);
			$this->session->set_userdata('import', $sess_data);

			//return $cnt;
		}

		function insert_images_dirxx()
		{

			$path = $this->input->post('folder');

			$d = $this->prepare_data($path);

			$this->db->trans_start();

			$_datas = array_chunk($d['rs'], 300);

			foreach ($_datas as $key => $data) {
			    $this->db->insert_batch('biometric', $data);
			}

			$this->db->trans_complete();

			//$this->db->insert_batch('biometric', $d['rs']);

			return $d['cnt'];
		}

		function prepare_data($fp, $sf, $files)
		{
			//$this->load->helper('directory');

			//$f = 'C:\Users\EC\Desktop\10120170911203716';

			//$map = directory_map($f);

			$cnt = 0;
			$rs = array();
			$data = array();
			$sfn = strstr($sf, '\\', TRUE);

			//foreach ($map as $k => $v) {
				
			 
			  foreach ($files as $file) {
				  $fn = pathinfo($file, PATHINFO_FILENAME);

				  if($fn=='signature') {
				  	$lfn = $fn;
				  }
				  else {
						$lfn = strstr($fn, '_');
				  }

					$img = addslashes(file_get_contents($fp.'/'.$sf.$file));
					$img = base64_encode($img);

					switch ($lfn) {
					    case "_RT":
					        $row['wsq_rt'] = $img;
					        break;
					    case "_RI":
					        $row['wsq_ri'] = $img;
					        break;
					    case "_RM":
					        $row['wsq_rm'] = $img;
					        break;
					    case "_RR":
					        $row['wsq_rr'] = $img;
					        break;
					   case "_RL":
					       $row['wsq_rl'] = $img;
					       break;
					   case "_LT":
					        $row['wsq_lt'] = $img;
					        break;
					    case "_LI":
					        $row['wsq_li'] = $img;
					        break;
					    case "_LM":
					        $row['wsq_lm'] = $img;
					        break;
					    case "_LR":
					        $row['wsq_lr'] = $img;
					        break;
					   case "_LL":
					       $row['wsq_ll'] = $img;
					       break;
					   case "_photo":
					       $row['photo'] = $img;
					       break;
					   case "signature":
					       $row['signature'] = $img;
					       break;
						}


				}

				$row['r_id'] = $sfn;
				$row['u_id'] = $this->generate_random_id();
				$row['created_on'] = date('Y-m-d H:i:s');

			//}

			return $row;
		}

		function generate_random_id()
		{
			$this->load->helper('string');
			$s = '-';

			$rn1 = random_string('numeric', 8);
			$rn1 = $rn1.$s;

			$rn2 = random_string('alnum', 4);
			$rn2 = $rn2.$s;

			$rn3 = random_string('alnum', 4);
			$rn3 = $rn3.$s;

			$rn4 = random_string('alnum', 4);
			$rn4 = $rn4.$s;

			$rn5 = random_string('alnum', 12);

			$rn = $rn1.$rn2.$rn3.$rn4.$rn5;

			return $rn;

		}

		function generate_random_idxx()
		{
			$this->load->helper('string');

			$rn = random_string('md5', 32);

			return $rn;

		}

	}
	?>